module git.lug.ustc.edu.cn/kafuluote/proto

go 1.13

require (
	github.com/golang/protobuf v1.4.3 // indirect
	github.com/micro/go-micro v1.18.0 // indirect
	github.com/micro/go-micro/v2 v2.9.1 // indirect
)
